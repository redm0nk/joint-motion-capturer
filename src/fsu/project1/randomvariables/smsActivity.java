package fsu.project1.randomvariables;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.gsm.SmsManager;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class smsActivity extends Activity {
	String sms_joint;
	String sms_timeout;
	String jointSelected;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.send_sms);
		Spinner spinner = (Spinner) findViewById(R.id.sms_joint);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				this, R.array.joints, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter);

		/** apply method on spinner to get the data **/

		spinner.setOnItemSelectedListener(new MyOnItemSelectedListener());

		/** set the spinner for the timeout selection **/

		Spinner spinner_1 = (Spinner) findViewById(R.id.sms_timeout);
		ArrayAdapter<CharSequence> adapter_1 = ArrayAdapter.createFromResource(
				this, R.array.testTimer, android.R.layout.simple_spinner_item);
		adapter_1
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner_1.setAdapter(adapter_1);

		/** apply the spinner method to get the data **/

		spinner_1.setOnItemSelectedListener(new MyOnItemSelectedListener2());
	}

	private void sendSMSIntent(String phoneNumber, String message) {
		PendingIntent pi = PendingIntent.getActivity(this, 0, new Intent(this,
				mainLogin.class), 0);
		SmsManager sms = SmsManager.getDefault();
		sms.sendTextMessage(phoneNumber, null, message, pi, null);
	}

	/** function which is called when the send sms button is hit **/

	public void sendSMS(View view) {

		String phone = null;
		String email = null;

		/** get the data for constructing the text message **/

		phone = ((EditText) findViewById(R.id.sms_phone)).getText().toString()
				.trim();
		email = ((EditText) findViewById(R.id.sms_email)).getText().toString()
				.trim();

		/** check for empty fields **/

		if (phone.length() == 0)
			showToast("please enter a phone number!");
		else if (email.length() == 0)
			showToast("please enter an email address!");
		else {
			/** send the SMS to the phone number specified **/

			String message = "StartThejointTest#" + sms_joint + "#"
					+ sms_timeout + "#" + email;
			sendSMSIntent(phone, message);

			showToast("sending message to " + phone);
		}
	}

	public void showToast(String msg) {
		Toast toastMsg = Toast.makeText(this, msg, Toast.LENGTH_LONG);
		toastMsg.setGravity(Gravity.BOTTOM, toastMsg.getXOffset() / 2,
				toastMsg.getYOffset() / 2);
		toastMsg.show();
	}

	/** class for getting the details from the spinner for the timeout **/
	// Defines On click actions for user list
	public class MyOnItemSelectedListener implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			if (parent.getItemAtPosition(pos).toString()
					.equals("Select a joint:") == false) {

				jointSelected = parent.getItemAtPosition(pos).toString();
			}

		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	class MyOnItemSelectedListener2 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			/*
			 * if (parent.getItemAtPosition(pos).toString()
			 * .equals("Select a joint:") == false) {
			 */

			sms_timeout = parent.getItemAtPosition(pos).toString();
			// }

		}

		public void onNothingSelected(AdapterView parent) {

			/** get the first timeout by default **/

			sms_timeout = parent.getItemAtPosition(0).toString();
		}
	}
}