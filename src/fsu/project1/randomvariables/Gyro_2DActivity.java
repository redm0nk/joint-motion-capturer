package fsu.project1.randomvariables;

import java.util.ArrayList;
import fsu.project1.randomvariables.GraphView.GraphViewData;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class Gyro_2DActivity extends Activity {
	// Global variable block
	public static final String TIME = "time";
	public static final String UID = "userName";
	public static final String JID = "joint";
	private GyroVisualizer mGyroView;
	private SensorManager mSensorManager;
	private Sensor mGyroSensor, mAccSensor;
	private ArrayList<GraphViewData> gyroVals;
	private ArrayList<GraphViewData> accVals;
	private static final float RADIANS_TO_DEGREES = (float) (180d / Math.PI);
	float maxAngle = 0, minAngle = 0;
	String username, joint;
	int T = 5, CALIBRATION_TIME = 2;
	private long startTime, endTime, calibrate_start, calibrate_end;
	private int flag = 0, toast_flag = 0;
	private float weight = 0.5f, angle_swap = 0;
	private String emailid = null;

	// computing euclidean distance between 2 points
	public double distance2D(float x1, float y1, float z1, float x2, float y2,
			float z2) {
		double dist = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)
				+ (z1 - z2) * (z1 - z2));
		return dist;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// arrays maintaining values.
		gyroVals = new ArrayList<GraphViewData>();
		accVals = new ArrayList<GraphViewData>();
		gyroVals.add(new GraphViewData(0f, 0f, 0f));
		accVals.add(new GraphViewData(0f, 0f, 0f));
		// intent to control time T.
		Intent intent = getIntent();
		T = intent.getIntExtra(TIME, 5);
		username = intent.getStringExtra(UID);
		joint = intent.getStringExtra(JID);
		weight = intent.getFloatExtra("weight", 0.5f);
		emailid = intent.getStringExtra("doctorEmail");

		// initializing Viewer class
		mGyroView = new GyroVisualizer(this);
		mGyroView.joint = joint;
		mGyroView.username = username;

		setContentView(mGyroView);
		// sensor manager stuffs.
		mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
		mGyroSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
		mAccSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	private float mRotationX, mRotationY, mRotationZ;
	private SensorEventListener mGyroListener = new SensorEventListener() {
		private static final float MIN_TIME_STEP = (1f / 40f);
		private long mLastTime = System.currentTimeMillis();

		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
		}

		@Override
		public void onSensorChanged(SensorEvent event) {
			float[] values = event.values;
			float x = values[0];
			float y = values[1];
			float z = values[2];
			float angularVelocity = z * 0.96f; // Minor adjustment to avoid
												// drift
			// Calculate time diff
			long now = System.currentTimeMillis();
			float timeDiff = (now - mLastTime) / 1000f;
			mLastTime = now;
			if (timeDiff > 1) {
				// Make sure we don't go bananas after pause/resume
				timeDiff = MIN_TIME_STEP;
			}

			// calculating angle of rotation
			mRotationX += x * timeDiff;
			if (mRotationX > 0.5f)
				mRotationX = 0.5f;
			else if (mRotationX < -0.5f)
				mRotationX = -0.5f;

			mRotationY += y * timeDiff;
			if (mRotationY > 0.5f)
				mRotationY = 0.5f;
			else if (mRotationY < -0.5f)
				mRotationY = -0.5f;
			mRotationZ -= angularVelocity * timeDiff;

			// publishing the value to the UI
			mGyroView.setGyroRotation(mRotationX, mRotationY, mRotationZ);

			GraphViewData temp = gyroVals.get(gyroVals.size() - 1);
			// if the Delta_angle > 5 degrees then push it inside vector array.
			if (distance2D(temp.valueX, temp.valueY, temp.valueZ, mRotationX
					* RADIANS_TO_DEGREES, mRotationY * RADIANS_TO_DEGREES,
					mRotationZ * RADIANS_TO_DEGREES) >= 5
					&& mGyroView.time_complete == false && mGyroView.take_test) {
				if (minAngle > mRotationZ * RADIANS_TO_DEGREES)
					minAngle = mRotationZ * RADIANS_TO_DEGREES;

				if (maxAngle < mRotationZ * RADIANS_TO_DEGREES)
					maxAngle = mRotationZ * RADIANS_TO_DEGREES;
				gyroVals.add(new GraphViewData(mRotationX * RADIANS_TO_DEGREES,
						mRotationY * RADIANS_TO_DEGREES, mRotationZ
								* RADIANS_TO_DEGREES));
			}

			buttonAction();
		}
	};
	
	int calibrate_flag = 0;
	Vibrator v;

	public void buttonAction() {
		// button actions ---------------------------------- start
		if (mGyroView.retake_test) {
			mGyroView.retake_test = false;
			flag = 0;
			minAngle = 0;
			maxAngle = 0;
			calibrate_flag = 0;
			toast_flag = 0;
			gyroVals.clear();
			accVals.clear();
			gyroVals.add(new GraphViewData(0f, 0f, 0f));
			accVals.add(new GraphViewData(0f, 0f, 0f));
		}
		if (mGyroView.show) {
			mGyroView.show = false;
			Intent i = new Intent(getApplicationContext(),
					ResultChartPlot.class);
			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			i.putParcelableArrayListExtra("GyroData", gyroVals);
			i.putParcelableArrayListExtra("AccData", accVals);
			i.putExtra(UID, username);
			i.putExtra(JID, joint);
			i.putExtra("doctorEmail", emailid);
			i.putExtra("weight", weight);
			i.putExtra("anglecovered", angle_swap);
			getApplicationContext().startActivity(i);
		}
		if (mGyroView.pressed_start) {
			if (calibrate_flag == 0) {
				calibrate_flag = 1;
				calibrate_start = System.currentTimeMillis();
			}
			calibrate_end = System.currentTimeMillis();

			if ((calibrate_end - calibrate_start) / 1000 >= CALIBRATION_TIME) {				
				mGyroView.take_test = true;

				if (flag == 0) {
					flag = 1;
					mRotationX = 0;
					mRotationY = 0;
					mRotationZ = 0;
					mGyroView.setGyroRotation(0f, 0f, 0f);
					gyroVals.clear();
					accVals.clear();
					gyroVals.add(new GraphViewData(0f, 0f, 0f));
					accVals.add(new GraphViewData(0f, 0f, 0f));
					
					startTime = System.currentTimeMillis(); // it is used to
															// calculate
															// the time
															// (T)
					// Get instance of Vibrator from current Context
					v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
					// Vibrate for 300 milliseconds
					v.vibrate(1000);
				}

				endTime = System.currentTimeMillis();
				if ((endTime - startTime) / 1000 >= T) {
					mGyroView.time_complete = true;
					mGyroView.take_test = false;

					if (toast_flag == 0) {
						toast_flag = 1;
						angle_swap = Math.round(maxAngle - minAngle);

						Toast.makeText(
								getApplicationContext(),
								"Range of Motion: "
										+ Math.round(angle_swap)
										+ " degrees", Toast.LENGTH_SHORT)
								.show();

						v.vibrate(1000);
					}
				}
			}
		}
		// button actions ---------------------------------- end
	}

	private SensorEventListener mAccListener = new SensorEventListener() {
		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
		}

		@Override
		public void onSensorChanged(SensorEvent event) {
			float[] values = event.values;
			float x = values[0];
			float y = values[1];
			float z = values[2];

			// Ignoring orientation since the activity is using
			// screenOrientation "nosensor"

			mGyroView.setAcceleration(-x, y, z);
			GraphViewData temp = accVals.get(accVals.size() - 1);
			if (distance2D(temp.valueX, temp.valueY, temp.valueZ, -x, y, z) >= 1
					&& mGyroView.time_complete == false) {
				accVals.add(new GraphViewData(-x, y, z));
			}
			buttonAction();
		}
	};

	@Override
	protected void onResume() {
		super.onResume();
		mSensorManager.registerListener(mGyroListener, mGyroSensor,
				SensorManager.SENSOR_DELAY_UI);
		mSensorManager.registerListener(mAccListener, mAccSensor,
				SensorManager.SENSOR_DELAY_UI);
	}

	@Override
	protected void onPause() {
		super.onPause();
		mSensorManager.unregisterListener(mGyroListener, mGyroSensor);
		mSensorManager.unregisterListener(mAccListener, mAccSensor);
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		mSensorManager.unregisterListener(mGyroListener, mGyroSensor);
		mSensorManager.unregisterListener(mAccListener, mAccSensor);
		finish();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		menu.add(50, 51, 0, "Help");
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == 51) {
			Intent i = new Intent(getApplicationContext(), helpActivity.class);
			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			getApplication().startActivity(i);
		}
		return true;
	}
}