package fsu.project1.randomvariables;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.StringTokenizer;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.sql.Timestamp;
import java.util.Date;
import fsu.project1.randomvariables.GraphView.GraphViewData;
import fsu.project1.randomvariables.GraphView.GraphViewSeries;
import fsu.project1.randomvariables.GraphView.LegendAlign;

/**
 * GraphViewDemo creates some dummy data to demonstrate the GraphView component.
 * 
 * IMPORTANT: For examples take a look at GraphView-Demos
 * (https://github.com/jjoe64/GraphView-Demos)
 * 
 * Copyright (C) 2011 Jonas Gehring Licensed under the GNU Lesser General Public
 * License (LGPL) http://www.gnu.org/licenses/lgpl.html
 */
public class ResultChartPlot extends Activity {
	/**
	 * @param savedInstanceState
	 */
	public ArrayList<GraphViewData> gyroVals;
	public ArrayList<GraphViewData> accVals;
	private Cursor constantsCursor = null;
	private DatabaseHelper db = null;
	String username, joint;
	Timestamp time_stamp;
	StringTokenizer st;
	double weight = 0.2;
	float angle_swap = 0;
	GraphViewData gyroData[];
	ArrayList<GraphViewData> final_gyroData;
	TextView angle_covered;
	String emaildoc = null, emailuser = null;
	Button save, profile;
	File file;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.graph_main);
		angle_covered = (TextView) findViewById(R.id.angle_display);
		Intent intent = getIntent();
		db = new DatabaseHelper(this);
		final_gyroData = new ArrayList<GraphView.GraphViewData>();
		constantsCursor = db.getReadableDatabase().rawQuery(
				"SELECT * " + "FROM tests ORDER BY uName", null);

		Date date = new Date();
		time_stamp = new Timestamp(date.getTime());
		gyroVals = intent.getParcelableArrayListExtra("GyroData");
		accVals = intent.getParcelableArrayListExtra("AccData");
		username = intent.getStringExtra(Gyro_2DActivity.UID);
		joint = intent.getStringExtra(Gyro_2DActivity.JID);
		weight = intent.getFloatExtra("weight", 0.5f) / 2;
		angle_swap = intent.getFloatExtra("anglecovered", 0f);
		emaildoc = intent.getStringExtra("doctorEmail");
		emailuser = intent.getStringExtra("userEmail");

		save = (Button) findViewById(R.id.button1);
		profile = (Button) findViewById(R.id.button2);

		angle_covered.setText("Angle covered: " + angle_swap + " degrees.");

		LineGraphView graphView = new LineGraphView(this, "Test result for "
				+ joint + ", time: " + time_stamp + ".");
		gyroData = new GraphViewData[gyroVals.size()];
		float max_accx = 1, max_accy = 1;
		for (int i = 0; i < accVals.size(); i++) {

			if (max_accx < accVals.get(i).valueX)
				max_accx = accVals.get(i).valueX;
			if (max_accy < accVals.get(i).valueY)
				max_accy = accVals.get(i).valueY;
		}

		int tpoint = accVals.size();

		for (int i = 0; i < gyroData.length; i++) {
			gyroData[i] = gyroVals.get(i);
			float angle = Math.abs(gyroData[i].valueZ)
					/ GyroVisualizer.RADIANS_TO_DEGREES;
			if (i >= tpoint)
				gyroData[i].valueY = (float) Math.pow(angle, 2);
			else {
				gyroData[i].valueY = (float) ((1 - weight) * Math.pow(angle, 2) + weight
						* Math.sqrt(Math.pow(accVals.get(i).valueX / max_accx,
								2)
								+ Math.pow(accVals.get(i).valueY / max_accy, 2)));
			}
			gyroData[i].valueX = i;
			final_gyroData.add(gyroData[i]);
		}

		graphView.addSeries(new GraphViewSeries("GyroScope Data", Color.rgb(0,
				0, 255), gyroData));
		// graphView.addSeries(new GraphViewSeries("Accelerometer Data - X",
		// Color
		// .rgb(255, 0, 0), accDataX));
		// graphView.addSeries(new GraphViewSeries("Accelerometer Data", Color
		// .rgb(0, 255, 0), accDataY));

		// graphView.setViewPort(0, 2);
		// graphView.setScalable(true);
		// graphView.setScrollable(true);

		graphView.setShowLegend(true);
		graphView.setLegendAlign(LegendAlign.BOTTOM);
		graphView.setLegendWidth(200);

		LinearLayout layout = (LinearLayout) findViewById(R.id.main);
		layout.addView(graphView);

		Button saveButton = (Button) findViewById(R.id.button1);
		saveButton.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {

				save.setVisibility(View.GONE);
				profile.setVisibility(View.GONE);

				// Getting view to bitmap
				// View v1 = L1.getRootView();
				// v1.setDrawingCacheEnabled(true);
				// Bitmap bm = v1.getDrawingCache();
				profile.setVisibility(View.VISIBLE);

				// ByteArrayOutputStream bos = new ByteArrayOutputStream();
				// bm.compress(CompressFormat.PNG, 0 /* ignored for PNG */,
				// bos);

				byte[] image = SerializerClass.serializeObject(final_gyroData);

				// byte[] image =
				// utilities.getBytes(BitmapFactory.decodeResource(
				// getResources(), R.drawable.graph3));
				ContentValues cv2 = new ContentValues(5);
				cv2.put(DatabaseHelper.UNAME, username);
				cv2.put(DatabaseHelper.JOINT, joint);
				st = new StringTokenizer((time_stamp).toString(), " ");
				String date_token = st.nextToken();
				String time_token = st.nextToken();
				time_token = time_token.substring(0, 5);
				String time_date = date_token + "_" + time_token;
				cv2.put(DatabaseHelper.TESTTIME, time_date);
				cv2.put(DatabaseHelper.GRAPH, image);
				cv2.put(DatabaseHelper.ANGLE, angle_swap);

				db.getWritableDatabase().insert("tests", DatabaseHelper.UNAME,
						cv2);
				constantsCursor.requery();
			}

		});
	}

	public void profileButtonPressed(View v) {
		Intent i = new Intent(getApplicationContext(), userProfile.class);
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		i.putExtra("userName", username);
		getApplicationContext().startActivity(i);

		db.close();
	}

	/*
	 * function for the display of an options menu this will hold the option to
	 * email
	 */

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(50, 54, 0, "Email Results");
		return true;
	}

	/* function for different menu options */

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == 54) {

			/****
			 * the bitmap image of the graph is mailed to a recipient. iView
			 * stores this image
			 */
			save.setVisibility(View.GONE);
			profile.setVisibility(View.GONE);
			LinearLayout l1 = (LinearLayout) findViewById(R.id.main);
			View v1 = l1.getRootView();
			v1.setDrawingCacheEnabled(true);
			Bitmap bm = v1.getDrawingCache();
			save.setVisibility(View.VISIBLE);
			profile.setVisibility(View.VISIBLE);

			/**** email code here *****/

			Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

			emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
					"Joint Force analysis data"); // subject line

			if (emaildoc != null)
				emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
						emaildoc); // recipients

			emailIntent.setType("image/png");

			/*** store image bmp temporarily on sd card ***/

			String extStorageDirectory = Environment
					.getExternalStorageDirectory().toString();
			OutputStream outStream = null;
			file = new File(extStorageDirectory, "test.PNG");

			try {
				if(file.exists()) file.delete();
				file.createNewFile();
			} catch (IOException e1) {
				Log.v("SD card error", "cannot create temp file");
			}

			try {
				outStream = new FileOutputStream(file);
				bm.compress(Bitmap.CompressFormat.PNG, 100, outStream);
				outStream.flush();
				outStream.close();
			} catch (Exception e) {
				Log.v("SD card error", "cannot save image to sd card");
			}

			emailIntent.putExtra(android.content.Intent.EXTRA_TEXT,
					"image data for force analysis graph testing attached"); // message
																				// body

			emailIntent.putExtra(
					Intent.EXTRA_STREAM,
					Uri.parse("file://"
							+ Environment.getExternalStorageDirectory()
									.toString() + "/test.PNG"));
			this.startActivity(Intent.createChooser(emailIntent, "Send email"));
		}

		return true;
	}
}
