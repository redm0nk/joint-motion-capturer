package fsu.project1.randomvariables;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import fsu.project1.randomvariables.GraphView.GraphViewData;
import fsu.project1.randomvariables.GraphView.GraphViewSeries;
import fsu.project1.randomvariables.GraphView.LegendAlign;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class resultComparision extends Activity {

	TextView vText;
	// Spinner vTestSpinner, vSpinnerJoints, vSpinnerSide, vSpinnerTimeout;
	ImageView iView;
	private DatabaseHelper db = null;
	private Cursor testsCursor = null;
	Toast toastMsg;
	String userName;
	Button button;

	@Override
	public void onCreate(Bundle icicle) {

		userName = getIntent().getExtras().getString("userName");
		super.onCreate(icicle);
		setContentView(R.layout.user_page);
		db = new DatabaseHelper(this);
		setContentView(R.layout.resultcompare);
		Intent it = getIntent();
		String userName = it.getStringExtra("userName");
		String jointSelected = it.getStringExtra("joint");
		button = (Button) findViewById(R.id.back_button);
		int lines[] = { Color.rgb(0, 0, 255), Color.rgb(0, 255, 0),
				Color.rgb(255, 0, 0) };

		testsCursor = db.getReadableDatabase().rawQuery(
				"SELECT * " + "FROM tests WHERE uName = '" + userName
						+ "' AND joint = '" + jointSelected
						+ "' ORDER BY testTime DESC", null);

		LineGraphView graphView = new LineGraphView(getApplicationContext(),
				"Result comparision of recent tests for joint: "+jointSelected);
		if (testsCursor.moveToFirst()) {
			for (int j = 0; j < 3; j++) {
				byte[] bb = testsCursor.getBlob(4);
				String time = testsCursor.getString(3);
				String angle = testsCursor.getString(5);
				if (bb != null) {
					ArrayList<GraphViewData> gyroData = (ArrayList<GraphViewData>) SerializerClass
							.deserializeObject(bb);

					GraphViewData plot_data[] = new GraphViewData[gyroData
							.size()];
					for (int i = 0; i < plot_data.length; i++)
						plot_data[i] = gyroData.get(i);

					if (plot_data != null) {
						graphView.addSeries(new GraphViewSeries(time + " : "
								+ angle + " deg", lines[j], plot_data));
					}
				}
				if (!testsCursor.moveToNext())
					break;
			}
		}

		graphView.setShowLegend(true);
		graphView.setLegendAlign(LegendAlign.BOTTOM);
		graphView.setLegendWidth(200);
		LinearLayout layout = (LinearLayout) findViewById(R.id.compare_chart);
		layout.addView(graphView);

		testsCursor.close();
		db.close();

	}

	public void backbutton(View v) {
		finish();
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		db.close();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		db.close();
	}@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(50, 54, 0, "Email Results");
		return true;
	}

	/* function for different menu options */

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == 54) {

			/****
			 * the bitmap image of the graph is mailed to a recipient. iView
			 * stores this image
			 */
			button.setVisibility(View.GONE);
			LinearLayout l1 = (LinearLayout) findViewById(R.id.compare_chart);
			View v1 = l1.getRootView();
			v1.setDrawingCacheEnabled(true);
			Bitmap bm = v1.getDrawingCache();
			button.setVisibility(View.VISIBLE);

			/**** email code here *****/

			Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

			emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
					"Joint Force analysis data"); // subject line

			emailIntent.setType("image/png");

			/*** store image bmp temporarily on sd card ***/

			String extStorageDirectory = Environment
					.getExternalStorageDirectory().toString();
			OutputStream outStream = null;
			File file = new File(extStorageDirectory, "test.PNG");

			try {
				if(file.exists()) file.delete();
				file.createNewFile();
			} catch (IOException e1) {
				Log.v("SD card error", "cannot create temp file");
			}

			try {
				outStream = new FileOutputStream(file);
				bm.compress(Bitmap.CompressFormat.PNG, 100, outStream);
				outStream.flush();
				outStream.close();
			} catch (Exception e) {
				Log.v("SD card error", "cannot save image to sd card");
			}

			emailIntent.putExtra(android.content.Intent.EXTRA_TEXT,
					"image data for force analysis graph testing attached"); // message
																				// body

			emailIntent.putExtra(
					Intent.EXTRA_STREAM,
					Uri.parse("file://"
							+ Environment.getExternalStorageDirectory()
									.toString() + "/test.PNG"));
			this.startActivity(Intent.createChooser(emailIntent, "Send email"));

		}

		return true;
	}
}
