package fsu.project1.randomvariables;

import java.io.ByteArrayOutputStream;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;

public class DatabaseHelper extends SQLiteOpenHelper {
	private static final String DATABASE_NAME = "db.db";
	static final String UNAME = "uName";
	static final String FNAME = "fName";
	static final String LNAME = "lName";
	static final String SEX = "sex";
	static final String BIRTHYEAR = "birthYear";
	static final String EMAIL = "eMail";

	// /////// Tests Table
	static final String JOINT = "joint";

	static final String TESTTIME = "testTime";
	static final String GRAPH = "graph";
	static final String ANGLE = "angle";

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE users (_id INTEGER PRIMARY KEY AUTOINCREMENT, uName TEXT, fName TEXT, lName TEXT, sex TEXT, birthYear TEXT, eMail TEXT);");
		db.execSQL("CREATE TABLE tests (_id INTEGER PRIMARY KEY AUTOINCREMENT, uName TEXT, joint TEXT, testTime TEXT, graph BLOB, angle TEXT);");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		android.util.Log.w("users",
				"Upgrading database, which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS constants");
		onCreate(db);
	}

	public static byte[] getBytes(Bitmap bitmap) {

		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bitmap.compress(CompressFormat.PNG, 0, stream);
		return stream.toByteArray();
	}
}