package fsu.project1.randomvariables;

import android.app.Activity;
import android.os.Bundle;
import java.util.StringTokenizer;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.telephony.SmsMessage;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class DoctorMSGActivity extends Activity {
	Button waitForDoctotButton;
	String Uname;
	/** Called when the activity is first created. */
	public static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.patientdoc);
		Uname = getIntent().getStringExtra("userName");
		waitForDoctotButton = (Button) findViewById(R.id.button_doctor);
		waitForDoctotButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				waitForDoctotButton.setText("waiting for the Doctor");
				waitForDoctor();
			}
		});
	}

	public void waitForDoctor() {
		IntentFilter filter = new IntentFilter(SMS_RECEIVED);

		registerReceiver(SMSBroadCastReceiver, filter);
	}

	BroadcastReceiver SMSBroadCastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			Bundle bundle = intent.getExtras();
			SmsMessage[] msgs = null;
			String doctorKeyword = "StartThejointTest";
			Boolean keyWordFound = false;
			String doctorMSG;
			String joint = null;
			String testTime = null;
			String doctorEmail = null;

			if (bundle != null && Uname != null) {

				Object[] pdus = (Object[]) bundle.get("pdus");
				msgs = new SmsMessage[pdus.length];
				for (int i = 0; i < msgs.length; i++) {
					msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);

					// // Search for keywords in received message /////
					if (msgs[i].getMessageBody().toString()
							.contains(doctorKeyword)) {
						keyWordFound = true;
						doctorMSG = msgs[i].getMessageBody().toString();
						StringTokenizer st = new StringTokenizer(doctorMSG, "#");
						st.nextToken(); // this one for the keyword
						joint = st.nextToken();
						testTime = st.nextToken();
						doctorEmail = st.nextToken();
					}

					if (keyWordFound) {
						unregisterReceiver(SMSBroadCastReceiver);
						SensorManager mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
						Sensor mGyroSensor = mSensorManager
								.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

						if (mGyroSensor == null) {
							Toast toastMsg = Toast.makeText(getApplicationContext(),
									"No GyroScope sensor detected",
									Toast.LENGTH_LONG);
							toastMsg.setGravity(Gravity.CENTER,
									toastMsg.getXOffset() / 2,
									toastMsg.getYOffset() / 2);
							toastMsg.show();
						} else {
							Intent inte = new Intent(context,
									Gyro_2DActivity.class);
							inte.putExtra(Gyro_2DActivity.UID, Uname);
							inte.putExtra(Gyro_2DActivity.JID, joint);
							inte.putExtra(Gyro_2DActivity.TIME, testTime);
							inte.putExtra("doctorEmail", doctorEmail);
							startActivity(inte);
						}
					}
				}
			}
		}
	};

	public void cancelButton(View view) {
		IntentFilter filter = new IntentFilter(SMS_RECEIVED);
		registerReceiver(SMSBroadCastReceiver, filter);
		unregisterReceiver(SMSBroadCastReceiver);
		finish();
	}
}
