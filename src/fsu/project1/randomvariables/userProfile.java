package fsu.project1.randomvariables;

import java.util.StringTokenizer;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class userProfile extends Activity {
	public static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
	TextView vText;
	// Spinner vTestSpinner, vSpinnerJoints, vSpinnerSide, vSpinnerTimeout;
	ImageView iView;
	private DatabaseHelper db = null;
	private Cursor usersCursor = null;
	private Cursor testsCursor = null;
	Toast toastMsg;
	String userName;
	byte[] bb;
	private String jointSelected, testSelected, timerSelected;
	private float weight = 0.5f;
	String userEmail = null;

	@Override
	public void onCreate(Bundle icicle) {

		userName = getIntent().getExtras().getString("userName");
		super.onCreate(icicle);
		setContentView(R.layout.user_page);
		db = new DatabaseHelper(this);
		usersCursor = db.getReadableDatabase().rawQuery(
				"SELECT * " + "FROM users WHERE uName = '" + userName + "'",
				null);
		testsCursor = db.getReadableDatabase().rawQuery(
				"SELECT * " + "FROM tests WHERE uName = '" + userName + "'",
				null);

		if (usersCursor != null && testsCursor != null) {
			if (usersCursor.moveToFirst()) {
				String firstName = usersCursor.getString(2);
				userEmail = usersCursor.getString(6);
				// bb = testsCursor.getBlob(5);
				vText = (TextView) findViewById(R.id.textView_welcomeUsertxt);
				vText.setText("Welcome " + firstName);
			}

			if (testsCursor.moveToFirst()) {
				bb = testsCursor.getBlob(4);
			}
			if (bb != null) {
				// iView = (ImageView) findViewById(R.id.imageView_graph);
				//
				// iView.setImageBitmap(BitmapFactory.decodeByteArray(bb, 0,
				// bb.length));
				usersCursor.close();
			}
		}
		// ///////////// test Spinner //////////////////////
		db = new DatabaseHelper(this);
		testsCursor = db.getReadableDatabase().rawQuery(
				"SELECT * " + "FROM tests WHERE uName = '" + userName + "'",
				null);
		String[] userTestsArray = new String[testsCursor.getCount()];
		if (testsCursor.moveToFirst()) {
			for (int i = 0; i < testsCursor.getCount(); i++) {
				userTestsArray[i] = testsCursor.getString(3) + " | "
						+ testsCursor.getString(2);
				testsCursor.moveToNext();
			}
		}
		testsCursor.close();

		Spinner vTestSpinner = (Spinner) findViewById(R.id.spinner_previousTests);
		ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, userTestsArray);
		vTestSpinner.setAdapter(adapter3);

		// ///////////// Other Spinners ///////////////////////
		String[] jointsArray = getResources().getStringArray(R.array.joints);
		Spinner vSpinnerJoints = (Spinner) findViewById(R.id.spinner_Joints);
		ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, jointsArray);
		adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		vSpinnerJoints.setAdapter(adapter1);

		String[] testTimerArray = getResources().getStringArray(
				R.array.testTimer);
		Spinner vSpinnerTimeout = (Spinner) findViewById(R.id.spinner_Test_Timer);

		ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, testTimerArray);
		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		vSpinnerTimeout.setAdapter(adapter2);

		vSpinnerJoints
				.setOnItemSelectedListener(new MyOnItemSelectedListener());
		vSpinnerTimeout
				.setOnItemSelectedListener(new MyOnItemSelectedListener2());
		vTestSpinner.setOnItemSelectedListener(new MyOnItemSelectedListener3());

		SeekBar seekbar = (SeekBar) findViewById(R.id.seekbar);

		seekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				// TODO Auto-generated method stub
				weight = progress * 0.01f;
			}

			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}

			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
		});
		usersCursor.close();
	}

	// Defines On click actions for user list
	public class MyOnItemSelectedListener implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			if (parent.getItemAtPosition(pos).toString()
					.equals("Select a joint:") == false) {

				jointSelected = parent.getItemAtPosition(pos).toString();
			}

		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	// Defines On click actions for results list
	public class MyOnItemSelectedListener2 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			if (parent.getItemAtPosition(pos).toString()
					.equals("Select a time:") == false) {

				timerSelected = parent.getItemAtPosition(pos).toString();				
			}

		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListener3 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			String temp = parent.getItemAtPosition(pos).toString();
			StringTokenizer st = new StringTokenizer(temp, "|");

			testSelected = st.nextToken().trim();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public void startTest(View view) {
		if (jointSelected == null || timerSelected == null) {
			toastMsg = Toast.makeText(this, "Please Select Joint and Timer",
					Toast.LENGTH_LONG);
			toastMsg.setGravity(Gravity.CENTER, toastMsg.getXOffset() / 2,
					toastMsg.getYOffset() / 2);
			toastMsg.show();
		} else {

			SensorManager mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
			Sensor mGyroSensor = mSensorManager
					.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

			if (mGyroSensor == null) {
				toastMsg = Toast.makeText(this, "No GyroScope sensor detected",
						Toast.LENGTH_LONG);
				toastMsg.setGravity(Gravity.CENTER, toastMsg.getXOffset() / 2,
						toastMsg.getYOffset() / 2);
				toastMsg.show();
			} else {

				// / Your activity where you start the test and create the graph
				// //////////
				Intent intent = new Intent(this, Gyro_2DActivity.class);
				intent.putExtra(Gyro_2DActivity.UID, userName);
				intent.putExtra(Gyro_2DActivity.JID, jointSelected);
				intent.putExtra(Gyro_2DActivity.TIME,
						Integer.parseInt(timerSelected));
				intent.putExtra("weight", weight);
				startActivity(intent);
			}
		}
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		usersCursor.close();
		testsCursor.close();
		db.close();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		testsCursor.close();
		usersCursor.close();
		db.close();
	}

	public void showResults(View view) {
		if (testSelected != null) {
			Intent i = new Intent(getApplicationContext(), showResults.class);
			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			i.putExtra("userName", userName);
			i.putExtra(Gyro_2DActivity.JID, jointSelected);
			i.putExtra("test_selected", testSelected);
			getApplicationContext().startActivity(i);
		} else {
			toastMsg = Toast.makeText(this,
					"Error! Select Joint or choose valid entry",
					Toast.LENGTH_SHORT);
			toastMsg.setGravity(Gravity.CENTER, toastMsg.getXOffset() / 2,
					toastMsg.getYOffset() / 2);
			toastMsg.show();
		}
	}

	public void showProgress(View view) {
		if (jointSelected == null) {
			toastMsg = Toast.makeText(this, "Please Select Joint",
					Toast.LENGTH_SHORT);
			toastMsg.setGravity(Gravity.CENTER, toastMsg.getXOffset() / 2,
					toastMsg.getYOffset() / 2);
			toastMsg.show();
		} else {
			Intent i = new Intent(getApplicationContext(),
					resultComparision.class);
			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			i.putExtra("userName", userName);
			i.putExtra("joint", jointSelected);
			getApplicationContext().startActivity(i);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		menu.add(50, 52, 0, "Connect to Doctor");
		menu.add(50, 51, 0, "Help");
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == 51) {
			Intent i = new Intent(getApplicationContext(), helpActivity.class);
			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			getApplication().startActivity(i);
		}
		if (id == 52) {
			Intent i = new Intent(getApplicationContext(),
					DoctorMSGActivity.class);
			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			i.putExtra("userName", userName);
			getApplication().startActivity(i);
		}
		return true;
	}
}
