package fsu.project1.randomvariables;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import fsu.project1.randomvariables.GraphView.GraphViewData;
import fsu.project1.randomvariables.GraphView.GraphViewSeries;
import fsu.project1.randomvariables.GraphView.LegendAlign;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

public class showResults extends Activity {
	// Global variable block
	private Cursor testsCursor = null;
	private DatabaseHelper db = null;
	byte[] bb;
	Button button;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.showresultsuser);
		db = new DatabaseHelper(this);
		Intent it = getIntent();
		String userName = it.getStringExtra("userName");
		String testSelected = it.getStringExtra("test_selected");
		String joint = it.getStringExtra(Gyro_2DActivity.JID);
		button = (Button) findViewById(R.id.button1);

		testsCursor = db.getReadableDatabase().rawQuery(
				"SELECT * " + "FROM tests WHERE testTime = '" + testSelected
						+ "' and uName = '" + userName + "'", null);
		if (testsCursor.moveToFirst()) {
			bb = testsCursor.getBlob(4);
		}
		if (bb != null) {
			ArrayList<GraphViewData> gyroData = (ArrayList<GraphViewData>) SerializerClass
					.deserializeObject(bb);
			LineGraphView graphView = new LineGraphView(
					getApplicationContext(), "Test result for joint: " + joint);
			GraphViewData plot_data[] = new GraphViewData[gyroData.size()];
			for (int i = 0; i < plot_data.length; i++) {
				plot_data[i] = gyroData.get(i);
			}
			if (plot_data != null) {
				String time = testsCursor.getString(3);
				String angle = testsCursor.getString(5);
				graphView.addSeries(new GraphViewSeries(time + " : " + angle
						+ " deg", Color.rgb(0, 0, 255), plot_data));
				graphView.setShowLegend(true);
				graphView.setLegendAlign(LegendAlign.BOTTOM);
				graphView.setLegendWidth(200);

				LinearLayout layout = (LinearLayout) findViewById(R.id.result_graph);
				layout.addView(graphView);
			}
		}else{
			Toast toastMsg = Toast.makeText(this, "No record exists",
					Toast.LENGTH_LONG);
			toastMsg.setGravity(Gravity.CENTER, toastMsg.getXOffset() / 2,
					toastMsg.getYOffset() / 2);
			toastMsg.show();
		}
		testsCursor.close();
		db.close();
	}

	public void backbutton(View v) {
		finish();
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		finish();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(50, 54, 0, "Email Results");
		return true;
	}

	/* function for different menu options */

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == 54) {

			/****
			 * the bitmap image of the graph is mailed to a recipient. iView
			 * stores this image
			 */
			button.setVisibility(View.GONE);
			LinearLayout l1 = (LinearLayout) findViewById(R.id.result_graph);
			View v1 = l1.getRootView();
			v1.setDrawingCacheEnabled(true);
			Bitmap bm = v1.getDrawingCache();
			button.setVisibility(View.VISIBLE);

			/**** email code here *****/

			Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

			emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
					"Joint Force analysis data"); // subject line

			emailIntent.setType("image/png");

			/*** store image bmp temporarily on sd card ***/

			String extStorageDirectory = Environment
					.getExternalStorageDirectory().toString();
			OutputStream outStream = null;
			File file = new File(extStorageDirectory, "test.PNG");

			try {
				if(file.exists()) file.delete();
				file.createNewFile();
			} catch (IOException e1) {
				Log.v("SD card error", "cannot create temp file");
			}

			try {
				outStream = new FileOutputStream(file);
				bm.compress(Bitmap.CompressFormat.PNG, 100, outStream);
				outStream.flush();
				outStream.close();
			} catch (Exception e) {
				Log.v("SD card error", "cannot save image to sd card");
			}

			emailIntent.putExtra(android.content.Intent.EXTRA_TEXT,
					"image data for force analysis graph testing attached"); // message
																				// body

			emailIntent.putExtra(
					Intent.EXTRA_STREAM,
					Uri.parse("file://"
							+ Environment.getExternalStorageDirectory()
									.toString() + "/test.PNG"));
			this.startActivity(Intent.createChooser(emailIntent, "Send email"));

		}

		return true;
	}
}