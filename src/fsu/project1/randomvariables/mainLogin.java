package fsu.project1.randomvariables;

import android.content.Intent;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class mainLogin extends Activity {
	private DatabaseHelper db = null;
	private Cursor constantsCursor = null;
	
	
	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		setContentView(R.layout.main);

	}

	public void login(View view) {
		final Intent intent = new Intent(this, userProfile.class);
		db = new DatabaseHelper(this);
		constantsCursor = db.getReadableDatabase().rawQuery(
				"SELECT * " + "FROM users ORDER BY uName", null);
		String[] userNamesArray = new String[constantsCursor.getCount()];
		if (constantsCursor.moveToFirst()) {
			for (int i = 0; i < constantsCursor.getCount(); i++) {
				userNamesArray[i] = constantsCursor.getString(1);
				constantsCursor.moveToNext();
			}
		}
		constantsCursor.close();

		final CharSequence[] items = userNamesArray;
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Select Your Username");
		builder.setItems(items, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				intent.putExtra("userName", items[item]);
				startActivity(intent);
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
		db.close();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		menu.add(50,53, 0, "Login as Doctor");
		menu.add(50, 51, 0, "Help");
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if(id == 51){
			Intent i = new Intent(getApplicationContext(), helpActivity.class);
			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			getApplication().startActivity(i);
		}
		if(id == 53){
			Intent i = new Intent(getApplicationContext(), smsActivity.class);
			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			getApplication().startActivity(i);
		}
		return true;
	}

	public void newUser(View view) {
		Intent inte = new Intent(this, addUser.class);
		startActivity(inte);
		finish();
	}

	public void exit(View view) {
		finish();
	}
}
