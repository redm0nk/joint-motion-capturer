package fsu.project1.randomvariables;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class graphPlot extends View {

	private static final float RADIANS_TO_DEGREES = (float) (180d / Math.PI);
	private static final float RADIUS = 150;

	private Paint mGyroPaint = new Paint();
	private Paint mAccPaint = new Paint();
	private Paint startTouchPaint = new Paint();
	private Paint pressed_startTouchPaint = new Paint();

	private float mGyroRotationX, mGyroRotationY, mGyroRotationZ;
	private float mAccX, mAccY, mAccZ, start_x, start_y;
	public boolean pressed_start, time_complete, retake_test, show;

	public graphPlot(Context context) {
		this(context, null);
	}

	public graphPlot(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public graphPlot(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setFocusable(true);

		pressed_start = false;
		time_complete = false;
		retake_test = false;
		show = false;

		mGyroPaint.setColor(0x77ffffff);
		mGyroPaint.setStyle(Style.STROKE);
		mGyroPaint.setStrokeWidth(5);
		mGyroPaint.setAntiAlias(true);

		mAccPaint.setColor(0xff33bb33);
		mAccPaint.setStrokeWidth(5);
		mAccPaint.setAntiAlias(true);

		startTouchPaint.setColor(Color.rgb(0, 0, 255));
		startTouchPaint.setStyle(Style.FILL_AND_STROKE);
		startTouchPaint.setStrokeWidth(5);
		startTouchPaint.setAntiAlias(true);

		pressed_startTouchPaint.setColor(Color.rgb(0, 0, 120));
		pressed_startTouchPaint.setStyle(Style.FILL_AND_STROKE);
		pressed_startTouchPaint.setStrokeWidth(5);
		pressed_startTouchPaint.setAntiAlias(true);
	}

	@Override
	public void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		Paint paint = new Paint();
		paint.setColor(Color.BLACK);
		paint.setStyle(Style.STROKE);
		canvas.drawPaint(paint);
		paint.setColor(Color.WHITE);
		paint.setTextSize(20);
		canvas.drawText("Angle: " + -1 * mGyroRotationZ * RADIANS_TO_DEGREES,
				10, 25, paint);

		float midX = getWidth() / 2f;
		float midY = getHeight() / 2f;
		canvas.drawLine(midX - RADIUS, midY + 12 + 15, midX, midY + 10,
				mGyroPaint);
		canvas.drawCircle(midX, midY, 14, mGyroPaint);
		canvas.drawLine(midX - RADIUS, midY - 12 - 15, midX, midY - 10,
				mGyroPaint);

		// Accelerometer
		float accX = midX + mAccX * 9.8f;
		float accY = midY + mAccY * 9.8f;
		canvas.drawLine(midX, midY, accX, accY, mAccPaint);
		canvas.drawCircle(accX, accY, 5, mAccPaint);

		// Gyroscope
		canvas.save();
		canvas.rotate(mGyroRotationZ * RADIANS_TO_DEGREES, midX, midY);
		canvas.drawLine(midX + 6, midY + RADIUS, midX + 12, midY, mGyroPaint);
		canvas.drawLine(midX - 6, midY + RADIUS, midX - 12, midY, mGyroPaint);
		// canvas.drawLine(midX - RADIUS, midY, midX + RADIUS, midY,
		// mGyroPaint);
		canvas.drawCircle(midX, midY, RADIUS, mGyroPaint);
		RectF rect = new RectF(midX - 15, midY + RADIUS, midX + 40, midY
				+ RADIUS + 20);
		canvas.drawOval(rect, mGyroPaint);
		canvas.restore();

		canvas.drawCircle(midX + mGyroRotationY * 350, midY + mGyroRotationX
				* 350, 10, mGyroPaint);

		// startButton
		start_x = midX;
		start_y = getHeight() * .9f;
		RectF startButton = new RectF(start_x - 50, start_y - 30, start_x + 50,
				start_y + 30);

		Paint startButtonText = new Paint();
		startButtonText.setColor(Color.WHITE);
		startButtonText.setTextSize(20);
		startButtonText.setStrokeWidth(2);

		if (!pressed_start) {
			canvas.drawRoundRect(startButton, 5, 5, startTouchPaint);
			canvas.drawText("START", start_x - 30, start_y + 5, startButtonText);
		} else {
			if (time_complete) {
				RectF retake = new RectF(start_x - 200, start_y - 30,
						start_x - 100, start_y + 30);
				RectF show = new RectF(start_x + 100, start_y - 30,
						start_x + 200, start_y + 30);
				canvas.drawRoundRect(retake, 5, 5, startTouchPaint);
				canvas.drawText("RE-TAKE", start_x - 185, start_y + 5,
						startButtonText);

				canvas.drawRoundRect(show, 5, 5, startTouchPaint);
				canvas.drawText("SHOW", start_x + 120, start_y + 5,
						startButtonText);
			} else {
				canvas.drawRoundRect(startButton, 5, 5, pressed_startTouchPaint);
				canvas.drawText("START", start_x - 30, start_y + 5,
						startButtonText);
			}
		}

		invalidate();
	}

	public void setGyroRotation(float x, float y, float z) {
		mGyroRotationX = x;
		mGyroRotationY = y;
		mGyroRotationZ = z;
	}

	public void setAcceleration(float x, float y, float z) {
		mAccX = x;
		mAccY = y;
		mAccZ = z;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		float x, y;
		x = event.getX();
		y = event.getY();
		if (y >= (start_y - 30) && y <= (start_y + 30)) {
			if (x >= (start_x - 50) && x <= (start_x + 50)
					&& pressed_start == false) {// clicked start button
				pressed_start = true;
			}
			if (time_complete == true && x >= (start_x - 200)
					&& x <= (start_x - 100)) {// clicked retake button
				pressed_start = false;
				time_complete = false;
				retake_test = true;
			}
			if (time_complete == true && x >= (start_x + 100)
					&& x <= (start_x + 200)) {// clicked retake button
				pressed_start = false;
				time_complete = false;
				show = true;
			}
		}
		return true;
	}
}
