package fsu.project1.randomvariables;

import android.os.Bundle;
import android.app.ListActivity;
import android.database.Cursor;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.SimpleCursorAdapter;

public class formsList extends ListActivity {
	private static final int DELETE_ID = Menu.FIRST + 3;
	private static final int CANCEL_DELETE_ID = Menu.FIRST + 4;
	private DatabaseHelper db = null;
	private Cursor constantsCursor = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		db = new DatabaseHelper(this);
		constantsCursor = db.getReadableDatabase().rawQuery(
				"SELECT _ID, uName, fName, lName, sex, eMail " + "FROM users",
				null);

		ListAdapter adapter = new SimpleCursorAdapter(this, R.layout.row,
				constantsCursor, new String[] { DatabaseHelper.UNAME,
						DatabaseHelper.FNAME, DatabaseHelper.LNAME,
						DatabaseHelper.SEX, DatabaseHelper.EMAIL }, new int[] {
						R.id.textView1, R.id.textView2, R.id.textView3,
						R.id.textView4, R.id.textView5 });

		setListAdapter(adapter);
		registerForContextMenu(getListView());
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		constantsCursor.close();
		db.close();
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenu.ContextMenuInfo menuInfo) {
		menu.add(Menu.NONE, DELETE_ID, Menu.NONE, "Delete")
				.setAlphabeticShortcut('d');
		menu.add(Menu.NONE, CANCEL_DELETE_ID, Menu.NONE, "Cancel")
				.setAlphabeticShortcut('c');
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case DELETE_ID: {
			AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
					.getMenuInfo();

			// delete(info.id);
			if (info.id > 0) {
				processDelete(info.id);
				return (true);
			}
		}
		case CANCEL_DELETE_ID:
		}

		return (super.onOptionsItemSelected(item));
	}

	private void processDelete(long rowId) {
		String[] args = { String.valueOf(rowId) };

		db.getWritableDatabase().delete("users", "_ID=?", args);
		constantsCursor.requery();
	}

}