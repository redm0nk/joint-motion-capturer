package fsu.project1.randomvariables;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

public class helpActivity extends Activity {

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		setContentView(R.layout.helplayout);
	}

	public void backbutton(View v) {
		finish();
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		finish();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		finish();
	}
}