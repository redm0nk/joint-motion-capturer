//// Source http://code.google.com/p/android-newbie-sourcecode/source/browse/trunk/ImageToDatabase/src/pete/android/study/Utilities.java

package fsu.project1.randomvariables;

import java.io.ByteArrayOutputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.CompressFormat;

public class utilities {
        public static byte[] getBytes(Bitmap bitmap) {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(CompressFormat.PNG, 0, stream);
                return stream.toByteArray();
        }
        
        public static Bitmap getImage(byte[] image) {
                return BitmapFactory.decodeByteArray(image, 0, image.length);
        }
}