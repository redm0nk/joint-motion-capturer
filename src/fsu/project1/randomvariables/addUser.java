package fsu.project1.randomvariables;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.view.Gravity;
import android.view.ViewGroup;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.RadioGroup;
import android.widget.Toast;

public class addUser extends Activity {
	Button btn_submit, btn_clear;
	EditText eText;
	TextView vText;
	RadioGroup rGroup;
	RadioButton rButton;
	Toast toastMsg;
	ViewGroup vGroup;
	boolean emptyField;

	private DatabaseHelper db=null;
	private Cursor constantsCursor=null;

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		setContentView(R.layout.main_table);
		emptyField = false;
		vGroup =(ViewGroup)findViewById(R.id.Layout_main);

		db=new DatabaseHelper(this);
		constantsCursor=db
		.getReadableDatabase()
		.rawQuery("SELECT * "+
				"FROM users ORDER BY uName",
				null); 

	}

	public void submitForm(View view){

		checkEmptyFields(vGroup);
		if (emptyField){
			showToast("Please fill empty fields");
			emptyField = false;
		}
		else
			if (fieldMatch()
					& checkUserName((EditText)findViewById(R.id.editText_userName))) 
			{
				showToast("Thank you, Your data was successfully sent");
				addForm();
				clearForm(view);
				

			}
	}

	//// Check if Username already exist or not /////////
	public boolean checkUserName(EditText edit_name){


		eText = (EditText)findViewById(R.id.editText_userName);
		String unameField = eText.getText().toString();
		db=new DatabaseHelper(this);
		Cursor matchCursor=db
		.getReadableDatabase()
		.rawQuery("SELECT * "+
				"FROM users WHERE uName = '"+unameField+"'",
				null); 

		if (matchCursor.getCount()>0){
			changeViewTextColor((TextView)findViewById(R.id.textView_userName),-65536 );
			showToast("User Name already exist");
			return false;
		}
		else
			return true;

	}

	/////// Check for any empty fields ///////////
	public void checkEmptyFields(ViewGroup group){
		ViewGroup vGroupSearch;
		for (int i = 0, count = group.getChildCount(); i < count; ++i) {
			View view = group.getChildAt(i);
			if (view instanceof EditText) {

				if (((EditText)view).getText().toString().trim().equals("")){
					vGroupSearch = (ViewGroup) view.getParent();
					emptyField = true;
					for (int j = 0, countj = vGroupSearch.getChildCount(); j < countj; ++j){
						View viewSearch = vGroupSearch.getChildAt(j);
						if (viewSearch instanceof TextView){
							((TextView)viewSearch).setTextColor(-65536);
						}
					}
				}
				else{
					vGroupSearch = (ViewGroup) view.getParent();

					for (int j = 0, countj = vGroupSearch.getChildCount(); j < countj; ++j){
						View viewSearch = vGroupSearch.getChildAt(j);
						if (viewSearch instanceof TextView){
							((TextView)viewSearch).setTextColor(-1);
						}
					}

				}
			}
			if(view instanceof ViewGroup && (((ViewGroup)view).getChildCount() > 0)){
				checkEmptyFields((ViewGroup)view);
			}			 
		}

		rGroup = (RadioGroup)findViewById(R.id.radioGroup_gender);
		if (rGroup.getCheckedRadioButtonId()== -1){
			changeViewTextColor((TextView)findViewById(R.id.textView_gender),-65536);
			emptyField =true;
		}
		else{
			changeViewTextColor((TextView)findViewById(R.id.textView_gender),-1);

		}
	}

	//////// Check if email/passowrd fields match or not //////////////
	public boolean fieldMatch(){
		boolean match = true;
		EditText eTextEmail = (EditText)findViewById(R.id.editText_Email);
		EditText eTextReEmail = (EditText)findViewById(R.id.editText_reEmail);

		////Before we do the emails match, we make sure the email is in valid format //////
		if (!checkValidEmail(eTextEmail.getText().toString())){
			changeViewTextColor((TextView)findViewById(R.id.textView_Email),-65536);
			showToast("Please enter a valid email address");
			match = false;
		}
		else{
			if (!eTextEmail.getText().toString().equals(eTextReEmail.getText().toString())) {
				changeViewTextColor((TextView)findViewById(R.id.textView_Email),-65536);
				changeViewTextColor((TextView)findViewById(R.id.textView_reEmail),-65536);
				showToast("Emails don't match");
				match = false;
			}
			else{
				changeViewTextColor((TextView)findViewById(R.id.textView_Email),-1 );
				changeViewTextColor((TextView)findViewById(R.id.textView_reEmail),-1 );
			}
		}
		//////////////Check Password Match ////////////
		
		if(match){
			return true;
		}
		else{
			return false;
		} 

	}
	//////// Clear the whole form /////////////
	public void clearForm (View view){
		rGroup = (RadioGroup)findViewById(R.id.radioGroup_gender);
		rGroup.clearCheck();		
		clearF(vGroup);
	}

	/////// Clear all EditText and turn TextView labels from red to white ///////////////
	public void clearF(ViewGroup group){	
		for (int i = 0, count = group.getChildCount(); i < count; ++i) {
			View view = group.getChildAt(i);
			if (view instanceof EditText) {
				((EditText)view).setText("");
			}

			if (view instanceof TextView){
				((TextView)view).setTextColor(-1);
			}

			if(view instanceof ViewGroup && (((ViewGroup)view).getChildCount() > 0)){
				clearF((ViewGroup)view);
			}


		}
	}

	///////Toast Msg ///////////////
	public void showToast(String msg){
		toastMsg = Toast.makeText(this, msg, Toast.LENGTH_LONG);
		toastMsg.setGravity(Gravity.CENTER, toastMsg.getXOffset() / 2, toastMsg.getYOffset() / 2);
		toastMsg.show();
	}

	/////////// Check if the email in a valid format /////////////
	public boolean checkValidEmail(String email)
	{
		Pattern pattern = Pattern.compile(".+@.+\\.[a-z]+");
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();

	}
	////////// Change the TextView next to the empty EditText to red or white //////////
	public void changeViewTextColor(TextView txtView, int colorCode){
		if (colorCode == -1){
			txtView.setTextColor(-1);
		}
		else{
			txtView.setTextColor(-65536);
		}

	}

	public void addForm(){

		ContentValues values=new ContentValues(6);

		eText = (EditText)findViewById(R.id.editText_userName);
		values.put(DatabaseHelper.UNAME, eText.getText().toString());
		
		eText = (EditText)findViewById(R.id.editText_First_Name);
		values.put(DatabaseHelper.FNAME, eText.getText().toString());
		
		eText = (EditText)findViewById(R.id.editText_Last_Name);
		values.put(DatabaseHelper.LNAME, eText.getText().toString());
		
		rButton = (RadioButton)findViewById(R.id.radio_male);
		if (rButton.isChecked()){		
		values.put(DatabaseHelper.SEX, "M");
		}
		else
			values.put(DatabaseHelper.SEX, "F");
		
		eText = (EditText)findViewById(R.id.editText_birthYear);
		values.put(DatabaseHelper.BIRTHYEAR, eText.getText().toString());
		
		eText = (EditText)findViewById(R.id.editText_Email);
		values.put(DatabaseHelper.EMAIL, eText.getText().toString());
		
		db.getWritableDatabase().insert("users", DatabaseHelper.UNAME, values);
		constantsCursor.requery();
	}
	@Override
	public void onDestroy() {
		super.onDestroy();

		constantsCursor.close();
		db.close();
	}
	
	
	public void cancelAdding(View view){
		Intent intent= new Intent(this, mainLogin.class);
		startActivity(intent);
		finish();
	}
	
	public void showList(View view){
		Intent intent= new Intent(this, formsList.class);

		startActivity(intent);
		
	}

}