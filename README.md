The joint motion detector (JMD) app performs a motion analysis of a specific joint. Using the gyroscope and accelerometer sensors on smartphones, data is collected and represented graphically. Similar to the tool Goniometer used for joint ROM (Range of Motion) assessment. JMD can be used to measure the motion range for knees, elbows and wrists. It also detects the patient’s vibration when he/she is moving the joint. This is important to notice if the joint is not moving smoothly. Patients can send their data to the doctor without actually having to show up at a hospital. It is beneficial for people with disability and elderly who have difficulty traveling or live far away from their doctors. The patient or the doctor can see the trend by comparing multiple results in a single graph. JMD can also receive a command from the doctor to start the test and send the results back to the doctor by email. This will be useful for telemedicine where the doctor can watch the patient over video chatting like Skype.

People who will most benefit from JMD:
Patients with Arthritis (a form of joint disorder that involves inflammation of one or more joints).
Doctors and people work in or have some issues related to physical therapy, protractor, joints, exercise, rehabilitation, anatomy.

Developer Team: TRV (The Random Variables)
-Aneesh Sharma
-Dean Burgos
-Murad Kablan
-Utsav Banerjee

How to use the JMD application:
1- Patient’ attaches the smartphone to his/her leg (close to the ankle) and starts the test. (This is for the knee test. In elbow and wrist tests, smart phone can be held in the hand).
2- The smart phone will count down to 5 seconds and then vibrates to alert the user to start moving the knee as showing in the help menu.
3- After T seconds (specified by the patient), the smart phone will save the graph’s results to send it later via email to the doctor attached with it the user’s name, time, joint tested.
4- The graph shall show the range in degrees. If user’s doesn’t reach a certain degree (specified by the doctor for each joint) or the line wasn’t smooth. This indicates a problem in the joint.
5- For more information please watch our videos
Testing elbow
http://www.youtube.com/watch?v=Al2QlG3KLyI&feature=endscreen&NR=1

Testing knee
http://www.youtube.com/watch?v=ydlpI1lzJIo

Doctor sending SMS to patient to start test
http://www.youtube.com/watch?v=b2v0-t-Mgmw

Android app link:
https://play.google.com/store/apps/details?id=trv.jmd